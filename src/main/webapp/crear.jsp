<%-- 
    Document   : crear
    Created on : 13-10-2020, 23:34:29
    Author     : crist
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Inscribir Jugador</title>
    </head>
    <body class="text-center" >
    <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
    
        </div>
      </header>
    </div>
      <main role="main" class="inner cover">
        <h1 class="cover-heading">Ingresar hardware</h1>
        <p class="">
            
   <form  name="form" action="MantencionHardware" method="POST">
                    <div class="form-group">
                        <label for="codigo">Codigo</label>
                        <input  name="codigo" class="form-control" required id="id" >
                           </div>
                    <br>
                    <div class="form-group">
                        <label for="nombre Producto">Nombre Producto</label>
                        <input  step="any" name="nombre" class="form-control" required id="nombre" >
                     </div>       
                    <br>
                    <div class="form-group">
                        <label for="tipodehardware">Tipo de hardware</label>
                         <input  name="tipo" class="form-control" required id="tipo" >
                 
                      </div>        
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="fabricante">Fabricante</label>
                         <input  name="fabricante" class="form-control" required id="fabricante" >
                 
                      </div>        
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="sku producto">Sku producto</label>
                         <input  name="sku" class="form-control" required id="sku" >
                 
                      </div>        
                    <br>
                    
                    
                    
                    <button type="submit" name="accion" value="inscribirGrabar" class="btn btn-success">Grabar</button>
                           <button type="submit" class="btn btn-success">Salir</button>
                </form>
  
      </main>

  
      
       
    </body>
</html>

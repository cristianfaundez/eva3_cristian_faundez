<%-- 
    Document   : registro
    Created on : 
    Author     : crist
--%>

<%@page import="java.util.Iterator"%>
<%@page import="cl.entities.Hardware"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Hardware> hardwares = (List<Hardware>) request.getAttribute("hardware");
    Iterator<Hardware> itHardware = hardwares.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Registro de Hardware</title>
    </head>
    <body class="text-center" >
        <form name="form" action="MantencionHardware" method="POST">  
   
            <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
                <table border="1">
                    <thead>
                    <th>Codigo</th>
                    <th>Nombre Producto </th>
                    <th>Tipo de hardware </th>
                    <th>Fabricante </th>
                    <th>Sku producto </th>
                    </thead>
                    <tbody>
                        <%while (itHardware.hasNext()) {
                       Hardware hard = itHardware.next();%>
                        <tr>
                            <td><%= hard.getHarId()%></td>
                            <td><%= hard.getHarNombre()%></td>
                            <td><%= hard.getHarTipo()%></td>
                            <td><%= hard.getHarFabricante()%></td>
                            <td><%= hard.getHarSku()%></td>

                            <td> <input type="radio" name="seleccion" value="<%= hard.getHarId()%>"> </td>
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
                <button type="submit" name="accion" value="editar" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">Editar</button>
                <button type="submit" name="accion" value="eliminar" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">Eliminar</button>
                <button type="submit" name="accion"  value="inscribir" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">inscribir</button>

        </form>
    </body>
</html>

<%-- 
    Document   : editar
    Created on : 14-10-2020, 2:39:20
    Author     : crist
--%>

<%@page import="cl.entities.Hardware"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
  Hardware Hardware=(Hardware)request.getAttribute("Hardware");
 
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Actualizar datos</title>
    </head>
    <body class="text-center" >
    <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
    
        </div>
      </header>
    </div>
      <main role="main" class="inner cover">
        <h1 class="cover-heading">Actualizar datos</h1>
        <p class="">
            
   <form  name="form" action="MantencionHardware" method="POST">
                    <div class="form-group">
                        <label for="codigo">Codigo</label>
                        <input  name="codigo" class="form-control"  value="<%= Hardware.getHarId()%>"  required id="id" >
                           </div>
                    <br>
                    <div class="form-group">
                        <label for="nombreproducto">Nombre producto</label>
                        <input  step="any" name="nombre" class="form-control"  value="<%= Hardware.getHarNombre()%>"  required id="nombre" >
                     </div>       
                    <br>
                    <div class="form-group">
                        <label for="tipodehardware">Tipo de hardware</label>
                         <input  name="tipo" class="form-control"  value="<%= Hardware.getHarTipo()%>"   required id="tipo" >
                 
                      </div>        
                    <br>
                     <div class="form-group">
                        <label for="fabricante">Fabricante</label>
                         <input  name="fabricante" class="form-control"  value="<%= Hardware.getHarFabricante()%>"   required id="fabricante" >
                 
                      </div>        
                    <br>
                    <div class="form-group">
                        <label for="sku">Sku producto</label>
                         <input  name="sku" class="form-control"  value="<%= Hardware.getHarSku()%>"   required id="fabricante" >
                 
                      </div>        
                    <br>
                   
                    <button type="submit" name="accion" value="inscribirActualizar" class="btn btn-success">Grabar</button>
                           <button type="submit" class="btn btn-success">Salir</button>
                </form>
  
      </main>

  
      
       
    </body>
</html>
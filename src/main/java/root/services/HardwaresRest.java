/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;


import cl.dao.HardwareJpaController;
import cl.dao.exceptions.NonexistentEntityException;
import cl.entities.Hardware;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;



@Path("/hardware")
public class HardwaresRest {
    
    EntityManagerFactory emf= Persistence.createEntityManagerFactory("hardDS");
    EntityManager em;
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo(){
       
     /* em = emf.createEntityManager();
        
        List<Hardware> lista = em.createNamedQuery("Hardware.findAll").getResultList(); */

       HardwareJpaController dao = new HardwareJpaController();
       List<Hardware> lista =dao.findHardwareEntities();
       System.out.println("lista.size():" +lista.size());
       return Response.ok(200).entity(lista).build();
    }
    
    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") String idbuscar) {
        HardwareJpaController dao = new HardwareJpaController();
        Hardware sel = dao.findHardware(idbuscar);
        return Response.ok(200).entity(sel).build();
        
    }
    
 @POST
     @Produces(MediaType.APPLICATION_JSON)
     public String nuevo(Hardware hardware) {
         HardwareJpaController dao = new HardwareJpaController();
         try {
            dao.create(hardware);
         } catch (Exception ex) {
             Logger.getLogger(HardwaresRest.class.getName()).log(Level.SEVERE, null, ex);            
         }
         return "Producto Guardado";
     }
     
     
     @PUT
     public Response actualizar(Hardware hardware){
     HardwareJpaController dao = new HardwareJpaController();
        try {
            dao.edit(hardware);
        } catch (Exception ex) {
            Logger.getLogger(HardwaresRest.class.getName()).log(Level.SEVERE, null, ex);
        }
     
         
         return Response.ok(200).entity(hardware).build();
     }
     
     
     @DELETE
     @Path("/{iddelete}")
     @Produces(MediaType.APPLICATION_JSON)
     public Response eliminaId(@PathParam("iddelete") String iddelete){
         
              HardwareJpaController dao = new HardwareJpaController();

        try {
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(HardwaresRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok("hardware eliminado").build();
    }
}







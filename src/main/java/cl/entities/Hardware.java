/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author crist
 */
@Entity
@Table(name = "hardware")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hardware.findAll", query = "SELECT h FROM Hardware h"),
    @NamedQuery(name = "Hardware.findByHarId", query = "SELECT h FROM Hardware h WHERE h.harId = :harId"),
    @NamedQuery(name = "Hardware.findByHarNombre", query = "SELECT h FROM Hardware h WHERE h.harNombre = :harNombre"),
    @NamedQuery(name = "Hardware.findByHarTipo", query = "SELECT h FROM Hardware h WHERE h.harTipo = :harTipo"),
    @NamedQuery(name = "Hardware.findByHarFabricante", query = "SELECT h FROM Hardware h WHERE h.harFabricante = :harFabricante"),
    @NamedQuery(name = "Hardware.findByHarSku", query = "SELECT h FROM Hardware h WHERE h.harSku = :harSku")})
public class Hardware implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "har_id")
    private String harId;
    @Size(max = 2147483647)
    @Column(name = "har_nombre")
    private String harNombre;
    @Size(max = 2147483647)
    @Column(name = "har_tipo")
    private String harTipo;
    @Size(max = 2147483647)
    @Column(name = "har_fabricante")
    private String harFabricante;
    @Size(max = 2147483647)
    @Column(name = "har_sku")
    private String harSku;

    public Hardware() {
    }

    public Hardware(String harId) {
        this.harId = harId;
    }

    public String getHarId() {
        return harId;
    }

    public void setHarId(String harId) {
        this.harId = harId;
    }

    public String getHarNombre() {
        return harNombre;
    }

    public void setHarNombre(String harNombre) {
        this.harNombre = harNombre;
    }

    public String getHarTipo() {
        return harTipo;
    }

    public void setHarTipo(String harTipo) {
        this.harTipo = harTipo;
    }

    public String getHarFabricante() {
        return harFabricante;
    }

    public void setHarFabricante(String harFabricante) {
        this.harFabricante = harFabricante;
    }

    public String getHarSku() {
        return harSku;
    }

    public void setHarSku(String harSku) {
        this.harSku = harSku;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (harId != null ? harId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hardware)) {
            return false;
        }
        Hardware other = (Hardware) object;
        if ((this.harId == null && other.harId != null) || (this.harId != null && !this.harId.equals(other.harId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.entities.Hardware[ harId=" + harId + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller;

import cl.entities.Hardware;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import utils.Config;


/**
 *
 * @author crist
 */
@WebServlet(name = "MantecionHardware", urlPatterns = {"/MantencionHardware"})
public class MantecionHardware extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MantecionHardware</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MantecionHardware at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion= request.getParameter("accion");
        
        if(accion.equals("inscribir")){
            
            request.getRequestDispatcher("crear.jsp").forward(request, response);

        }
        
        if(accion.equals("editar")){
            String seleccion = request.getParameter("seleccion");

            Config.getVariablesEntorno();
            Client client = ClientBuilder.newClient();
            WebTarget myResource = client.target(Config.host+"/" + seleccion);
            
            Hardware sel = myResource.request(MediaType.APPLICATION_JSON).get(Hardware.class);
            
            
            request.setAttribute("Hardware", sel);
            request.getRequestDispatcher("editar.jsp").forward(request, response);
            
        }
        
        if(accion.equals("eliminar")){
            
            String seleccion = request.getParameter("seleccion");
            
            
                        Config.getVariablesEntorno();

            Client client = ClientBuilder.newClient();
            WebTarget myResource = client.target(Config.host+"/" + seleccion);
            myResource.request(MediaType.APPLICATION_JSON).delete();

            
            
            Client client1 = ClientBuilder.newClient();
            WebTarget myResource1 = client1.target(Config.host);
            List<Hardware> sel = myResource1.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Hardware>>() {
            });

            request.setAttribute("hardware", sel);
            request.getRequestDispatcher("registro.jsp").forward(request, response);
            
            
        }
        
        if(accion.equals("inscribirGrabar")){
            
            String codigo= request.getParameter("codigo");
            String nombre= request.getParameter("nombre");
            String tipo= request.getParameter("tipo");
            String fabricante= request.getParameter("fabricante");
            String sku= request.getParameter("sku");
            
            
            
            
            
             Hardware nuevoHardware = new Hardware();
             
             nuevoHardware.setHarId(sku);
             nuevoHardware.setHarNombre(nombre);
             nuevoHardware.setHarSku(sku);
             nuevoHardware.setHarTipo(tipo);
             nuevoHardware.setHarFabricante(fabricante);
          
             Config.getVariablesEntorno();
            Client client = ClientBuilder.newClient();
            WebTarget myResource = client.target(Config.host);
            myResource.request(MediaType.APPLICATION_JSON).post(Entity.json(nuevoHardware));

            
            Client client1 = ClientBuilder.newClient();
            WebTarget myResource1 = client1.target(Config.host);
            List<Hardware> sel = myResource1.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Hardware>>() {
            });

            request.setAttribute("hardware", sel);
            request.getRequestDispatcher("registro.jsp").forward(request, response);

            

        
        }
        
        
        if(accion.equals("inscribirActualizar")){
            
            String codigo= request.getParameter("codigo");
            String nombre= request.getParameter("nombre");
            String tipo= request.getParameter("tipo");
            String fabricante= request.getParameter("fabricante");
            String sku= request.getParameter("sku");
            
            Hardware hardware=new Hardware();
            hardware.setHarId(codigo);
            hardware.setHarNombre(nombre);
            hardware.setHarTipo(tipo);
            hardware.setHarFabricante(fabricante);
            hardware.setHarSku(sku);
           
            Config.getVariablesEntorno();
            Client client1 = ClientBuilder.newClient();
            WebTarget myResource1 = client1.target(Config.host);
            myResource1.request(MediaType.APPLICATION_JSON).put(Entity.json(hardware), Hardware.class);

            
             Client client = ClientBuilder.newClient();
            WebTarget myResource = client.target(Config.host);

            
            List<Hardware> sel = myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Hardware>>() {
            });
            request.setAttribute("hardware", sel);

            request.getRequestDispatcher("registro.jsp").forward(request, response);
            
        
        
  }
}
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
       
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
